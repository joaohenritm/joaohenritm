<h1>Welcome to my Portfolio!</h1>

	
## ***João Henrique Tavares***
I'm 23 years old and the first time that I heard about Data Science was during my bachelor, more specifically in the econometrics subject, since then my interest in this area only has grown and now I am developing data products to resolve business problems.

<h3>Analytical Tools:</h3>
	
- **Data Collect and Storage:** SQL, SQLite, PostgreSQL, WebScraping.
	
- **Data Processing and Analysis:** Python, R.
	
- **Development:** Git, Linux.
	
- **Data Visualization:** Power BI, Streamlit.
	
- **Machine Learning Modelling:** Classification, Regression, Time Series, Clustering.
	
### Projects

| Start Date | Project Name | Link | Status |
| ---------- | ------------ | ---- | -------- |
| 2022-03-27 | House Rocket Analysis | https://gitlab.com/joaohenritm/House-Rocket | Finished |
| 2022-04-05 | Star Jeans Analysis | https://gitlab.com/joaohenritm/Star-Jeans | Populating the Database |
| 2022-04-19 | Rossmann Sales Forecast | https://gitlab.com/joaohenritm/Rossman | Finished |
| 2022-04-27 | Health Insurance Cross Sell | https://gitlab.com/joaohenritm/Health-Insurance-Cross-Sell | Finished | 
| 2022-05-05 | Imóveis em Olinda | https://gitlab.com/joaohenritm/Imoveis-Olinda | In development |
| 2022-05-15 | Loyalty Program - Insiders | https://gitlab.com/joaohenritm/Insiders-Clustering | In development | 
| 2022-05-30 | African Financial Accessibility (HACKDAY) | https://gitlab.com/joaohenritm/analise-bancaria-africa | Finished |
<!---
jaohenritm/jaohenritm is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
